﻿using System;
using System.Text;

namespace LongLib
{
    public static class LongArythmetics
    {
        public static string Addition(LongNumber addenum1, LongNumber addenum2)
        {
            var answer = new StringBuilder();
            var over = 0;
            var add1Len = addenum1.Length;
            var add2Len = addenum2.Length;
            var maxLen = Math.Max(add1Len, add2Len);

            if (addenum1.IsNegative && !addenum2.IsNegative)
                return Substraction(addenum2, addenum1.Module);

            if (!addenum1.IsNegative && addenum2.IsNegative)
                return Substraction(addenum1, addenum2.Module);

            if (addenum1.IsNegative && addenum2.IsNegative)
            {
                var ans = Addition(addenum1.Module, addenum2.Module);
                if (StringHelper.Compare(ans, "0") == 0)
                    return ans;

                return "-" + ans;
            }

            for (int i = 0; i < maxLen; i++)
            {
                var a1 = addenum1.CharIndex(i);
                var a2 = addenum2.CharIndex(i);

                var tmp = a1 + a2 + over;

                over = tmp / 10;
                answer.Append((tmp % 10).ToString());
            }

            if (over > 0) answer.Append(over);

           return StringHelper.Revert(answer.ToString());
        }

        public static string Substraction(LongNumber minuend, LongNumber subtrahend)
        {
            var answer = new StringBuilder();
            var occupy = 0;
            var minLen = minuend.Length;
            var subLen = subtrahend.Length;
            var maxLen = Math.Max(minLen, subLen);

            if (minuend.IsNegative && !subtrahend.IsNegative)
            {
                var ans = Addition(minuend.Module, subtrahend);
                if (StringHelper.Compare(ans, "0") == 0)
                    return ans;

                return "-" + ans;
            }

            if (!minuend.IsNegative && subtrahend.IsNegative)
                return Addition(minuend, subtrahend.Module);

            if (minuend.IsNegative && subtrahend.IsNegative)
                return Substraction(subtrahend.Module, minuend.Module);

            if (minuend.CompareTo(subtrahend) == -1)
            {
                var ans = Substraction(subtrahend, minuend);
                if (StringHelper.Compare(ans, "0") == 0)
                    return ans;

                return "-" + ans;
            }

            for (var i = 0; i < maxLen; i++)
            {
                var m = minuend.CharIndex(i);
                var s = subtrahend.CharIndex(i);

                int tmp = 10 + m - s + occupy;

                occupy = tmp / 10 - 1;
                answer.Append((tmp % 10).ToString());
            }

            var j = answer.Length - 1;
            while (j > 0 && answer[j] == '0')
                answer.Remove(j--, 1);

            return StringHelper.Revert(answer.ToString());
        }

        public static string Multiplexion(LongNumber multiplied, LongNumber factor)
        {
            var answer = new StringBuilder();
            var over = 0;

            if (factor.Length == 0)
                return "0";

            if ((multiplied.IsNegative && !factor.IsNegative) ||
                (!multiplied.IsNegative && factor.IsNegative))
            {
                var ans = Multiplexion(multiplied.Module, factor.Module);
                if (StringHelper.Compare(ans, "0") == 0)
                    return ans;

                return "-" + ans;
            }

            if (multiplied.IsNegative && factor.IsNegative)
                return Multiplexion(multiplied.Module, factor.Module);

            var mult = Multiplexion(multiplied, factor.SubNumber(1));

            var f = factor.LastCharIndex(0);
            for (var i = 1; i < factor.Length; i++)
                answer.Append("0");

            for (var i = 0; i < multiplied.Length; i++)
            {
                var m = multiplied.CharIndex(i);

                var tmp = m * f + over;

                over = tmp / 10;
                answer.Append((tmp % 10).ToString());
            }

            if (over > 0) answer.Append(over);

            var reversed = StringHelper.Revert(answer.ToString());

            return Addition(new LongNumber(mult), new LongNumber(reversed));
        }

        public static string Division(LongNumber devisible, LongNumber divisor)
        {
            var answer = new StringBuilder();

            if (divisor.CompareTo(new LongNumber("0")) == 0)
                throw new DivideByZeroException();
            
            if ((devisible.IsNegative && !divisor.IsNegative) ||
                (!devisible.IsNegative && divisor.IsNegative))
            {
                var ans = Division(devisible.Module, divisor.Module);
                if (StringHelper.Compare(ans, "0") == 0)
                    return ans;

                return "-" + ans;
            }

            if (devisible.IsNegative && divisor.IsNegative)
                return Division(devisible.Module, divisor.Module);

            var curDevisible = devisible.ToString();

            while (StringHelper.Compare(curDevisible, divisor) >= 0)
            {
                var tmpSub = "0";
                var subtrahend = new StringBuilder("0"); 
                var tmp = new StringBuilder(curDevisible.Substring(0, 
                                            divisor.Length));
                
                if (StringHelper.Compare(tmp, divisor) < 0)
                    tmp.Append(curDevisible[divisor.Length]);

                if (tmp.Length == 0)
                    tmp.Append(0);

                int i = 0;
                do
                {
                    i++;
                    subtrahend.Clear();
                    subtrahend.Append(tmpSub);
                    tmpSub = Multiplexion(new LongNumber(divisor),
                        new LongNumber(i));
                    
                } while (StringHelper.Compare(tmpSub, tmp) <= 0);

                answer.Append(--i);

                i = 0;
                while (curDevisible.Length - tmp.Length > i++)
                    subtrahend.Append("0");

                curDevisible = Substraction(new LongNumber(curDevisible),
                    new LongNumber(subtrahend));
            }

            if (answer.Length == 0)
                return "0";

            return answer.ToString();
        }
    }
}