﻿using System;
using System.Text;

namespace LongLib
{
    public class LongNumber : IComparable
    {

        public string Number { get; set; }

        public int Length => Number.Length;

        public bool IsNegative => Number[0] == '-';

        public int CharIndex(int index)
        {
            return (Number.Length > index) ? Number[Number.Length - index - 1] - '0' : 0;
        }

        public int LastCharIndex(int index)
        {
            return (Number.Length > index) ? Number[index] - '0' : 0;
        }

        public LongNumber Module => new LongNumber(IsNegative ? ToString().Substring(1) : Number);

        public LongNumber SubNumber(int index)
        {
            if (Number.Length < index) return new LongNumber("");
            return new LongNumber(Number.ToString().Substring(index));
        }

        public LongNumber(object number)
        {
            Number = number.ToString();
        }

        public LongNumber()
        {
            Number = "0";
        }

        public override string ToString()
        {
            return Number;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is LongNumber lnum))
                throw new TypeLoadException();

            if (IsNegative == false && lnum.IsNegative)
                return 1;

            if (IsNegative && lnum.IsNegative == false)
                return -1;

            if (Length > lnum.Length)
                return 1;

            if (Length < lnum.Length)
                return -1;

            return string.Compare(Number, lnum.ToString(), 
                        StringComparison.InvariantCulture);

        }

        public static implicit operator string(LongNumber longNumber)
        {
            return longNumber.ToString();
        }
    }
}
