﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LongLib
{
    public class StringHelper
    {
        public static string Revert(string strToRevert)
        {
            char[] arr = strToRevert.ToCharArray();
            Array.Reverse(arr);

            return new string(arr);
        }

        public static int Compare(Object string1, Object string2)
        {
            string str1 = string1.ToString();
            string str2 = string2.ToString();

            if (str1.Length > str2.Length)
                return 1;

            if (str2.Length > str1.Length)
                return -1;

            return String.Compare(str1, str2, StringComparison.InvariantCulture);
        }
    }
}
