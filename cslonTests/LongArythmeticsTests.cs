﻿using System;
using System.Numerics;
using LongLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace cslonTests
{

	public class CalculatesMethods
	{
		
		
		public static string CalculateActualAdd(LongNumber f, LongNumber s)
		{
			return (BigInteger.Parse(f) + BigInteger.Parse(s)).ToString();
		}
		
		public static string CalculateActualSub(LongNumber f, LongNumber s)
		{
			return (BigInteger.Parse(f) - BigInteger.Parse(s)).ToString();
		}

		public static string CalculateActualMult(LongNumber f, LongNumber s)
		{
			return (BigInteger.Parse(f) * BigInteger.Parse(s)).ToString();
		}

		public static string CalculateActualDiv(LongNumber f, LongNumber s)
		{
			return (BigInteger.Parse(f) / BigInteger.Parse(s)).ToString();
		}
	}
	
	[TestFixture("Addition")]
	[TestFixture("Substraction")]
	[TestFixture("Multiplexion")]
	[TestFixture("Division")]
	public class LongArythmeticsShouldCalculate
	{
		delegate string SimpleDelegate(LongNumber arg1, LongNumber arg2);
		private SimpleDelegate actLN;
		private SimpleDelegate actTest;

		public LongArythmeticsShouldCalculate(string func)
		{
			if (StringHelper.Compare(func, "Addition") == 0)
			{
				actLN = LongArythmetics.Addition;
				actTest = CalculatesMethods.CalculateActualAdd;
			}
			
			if (StringHelper.Compare(func, "Substraction") == 0)
			{
				actLN = LongArythmetics.Substraction;
				actTest = CalculatesMethods.CalculateActualSub;
			}
			
			if (StringHelper.Compare(func, "Multiplexion") == 0)
			{
				actLN = LongArythmetics.Multiplexion;
				actTest = CalculatesMethods.CalculateActualMult;
			}
			
			if (StringHelper.Compare(func, "Division") == 0)
			{
				actLN = LongArythmetics.Division;
				actTest = CalculatesMethods.CalculateActualDiv;
			}
		}

		[TestCase("3434", "12415412")]
		[TestCase("3434213", "12414")]
		[TestCase("94342", "12414")]
		[TestCase("3434", "-12415412")]
		[TestCase("3434213", "-12414")]
		[TestCase("94342", "-12414")]
		[TestCase("-3434", "12415412")]
		[TestCase("-3434213", "12414")]
		[TestCase("-94342", "12414")]
		[TestCase("-3434", "-12415412")]
		[TestCase("-3434213", "-12415412")]
		[TestCase("-94342", "-12414")]
		[TestCase("-9434234534534543636576", "-12414346436346346")]
		public void LNTest(string arg1, string arg2)
		{
			var first = new LongNumber(arg1);
			var second = new LongNumber(arg2);
			
			Assert.AreEqual(actLN(first, second), actTest(first, second));
		}
		
	}
}