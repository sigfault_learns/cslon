﻿using LongLib;
using System;

namespace cslon
{
    class Program
    {

        static void Main(string[] args)
        {
            LongNumber firstNumber = new LongNumber("15");
            LongNumber secondNumber = new LongNumber("-3");
            string operationType = "";

            Console.Write("First: ");
            firstNumber.Number = Console.ReadLine();

            Console.Write("Second: ");
            secondNumber.Number = Console.ReadLine();

            Console.Write("Operation: ");
            operationType = Console.ReadLine();
            Console.WriteLine();

            switch (operationType)
            {
                case "+":
                    Console.WriteLine(LongArythmetics.Addition(firstNumber, secondNumber));
                    break;
                case "-":
                    Console.WriteLine(LongArythmetics.Substraction(firstNumber, secondNumber));
                    break;
                case "*":
                    Console.WriteLine(LongArythmetics.Multiplexion(firstNumber, secondNumber));
                    break;
                case "/":
                    Console.WriteLine(LongArythmetics.Division(firstNumber, secondNumber));
                    break;
            }
        }

    }
}
